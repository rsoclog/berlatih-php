<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah Huruf</title>
</head>
<body>
    <?php
    function ubah($huruf){
        for ($i = 0; $i < strlen($huruf); $i++){
            $ganti[$i] = chr(ord($huruf[$i])+1);
        } return implode($ganti)."<br>";
    }
    echo ubah("wow");
    echo ubah("developer");
    echo ubah("laravel");
    echo ubah("keren");
    echo ubah("semangat");
    ?>
</body>
</html>