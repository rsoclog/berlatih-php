<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tentukan Nilai</title>
</head>
<body>
    <?php
    function tentukan_nilai($nilai){
        if ($nilai >= 85 && $nilai <= 100)
        {
            return "sangat baik<br>";
        }else if ($nilai >= 70 && $nilai <= 85)
        {
            return "baik<br>";
        }else if ($nilai >= 60 && $nilai <= 70)
        {
            return "cukup<br>";
        }else {
            return "kurang";
        }
    }
    echo tentukan_nilai(98);
    echo tentukan_nilai(76);
    echo tentukan_nilai(67);
    echo tentukan_nilai(43);
    ?>
</body>
</html>